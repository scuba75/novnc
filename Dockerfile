FROM python

RUN apt-get update -y

RUN apt-get upgrade -y

# Installing the fundamental package for scientific computing with Python: numpy
RUN apt-get install -y python-numpy

# clean the backup
RUN apt-get clean

# Copy the files into the container
ADD ./websockify/ /websockify/
ADD ./noVNC/ /noVNC/
# start the java application
CMD ["/websockify/run", "--web", "/noVNC", "6080" ,"--token-plugin", "TokenFile", "--token-source", "/websockify/token"]

# usage volume
# VOLUME ["/websockify/token/"]
